﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.propinasAluno
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class propinasAluno
  {
    [DataMember]
    public string anoLectivo;
    [DataMember]
    public string codCurso;
    [DataMember]
    public string curso;
    [DataMember]
    public string descr;
    [DataMember]
    public string estado;
    [DataMember]
    public string referencia;
    [DataMember]
    public string valor;
    [DataMember]
    public string datalimitepag;
    [DataMember]
    public string datapagamento;
    [DataMember]
    public string entidade;
    [DataMember]
    public string prestacao;
    [DataMember]
    public string numero;
    [DataMember]
    public string moeda;
    [DataMember]
    public string pendente;
    [DataMember]
    public string datadevolucao;
    [DataMember]
    public string dataanulacao;
  }
}
