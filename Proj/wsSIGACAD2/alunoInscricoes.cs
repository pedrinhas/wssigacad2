﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.alunoInscricoes
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class alunoInscricoes
  {
    [DataMember]
    public string TipoExame;
    [DataMember]
    public int IDMatricula;
    [DataMember]
    public string nomedisciplina;
    [DataMember]
    public double creditos;
    [DataMember]
    public double ects;
    [DataMember]
    public int anoLectivo;
    [DataMember]
    public int anoCurricular;
    [DataMember]
    public int semestre;
    [DataMember]
    public int codDisciplina;
    [DataMember]
    public string dataInscricao;
    [DataMember]
    public int primeira_vez;
    [DataMember]
    public int regAluno;
    [DataMember]
    public int validade;
    [DataMember]
    public int epocaexame;
    [DataMember]
    public string departamento;
    [DataMember]
    public int codCurso;
    [DataMember]
    public string curso;
  }
}
