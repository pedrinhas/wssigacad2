﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.alunoCursos
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class alunoCursos
  {
    [DataMember]
    public string codCurso;
    [DataMember]
    public string codPlano;
    [DataMember]
    public string idRamo;
    [DataMember]
    public string idMatricula;
    [DataMember]
    public string nomeCurso;
    [DataMember]
    public string siglaCurso;
  }
}
