﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.SIGACAD
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace wsSIGACAD2
{
  public class SIGACAD : ISIGACAD
  {
    private string myConnectionString = WebConfigKeys.ConnectionStrings.Apps2;

    public void DoWork()
    {
    }

    public DataTable lerRegime(string numMec)
    {
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand selectCommand = new SqlCommand())
        {
          selectCommand.CommandText = "stp_Regime_LS";
          selectCommand.Connection = sqlConnection;
          selectCommand.CommandType = CommandType.StoredProcedure;
          SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(selectCommand);
          DataTable dataTable = new DataTable("Registo");
          sqlDataAdapter.Fill(dataTable);
          return dataTable;
        }
      }
    }

    public List<Regime> GetAlunoRegime(string numMec)
    {
      List<Regime> regimeList = new List<Regime>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "stp_Regime_Aluno_S";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@NumeroIn", SqlDbType.Int).Value = (object) numMec;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          int num = 0;
          while (sqlDataReader.Read())
          {
            regimeList.Add(new Regime()
            {
              Numero = Convert.ToInt32(sqlDataReader["Numero"]),
              AnoLectivo = Convert.ToInt32(sqlDataReader["AnoLectivo"]),
              DataInicio = Convert.ToDateTime(sqlDataReader["DataInicio"]),
              CodRegime = Convert.ToString(sqlDataReader["CodRegime"]),
              NomeRegime = Convert.ToString(sqlDataReader["NomeRegime"]),
              CodCurso = Convert.ToString(sqlDataReader["CodCurso"]),
              NomeCurso = Convert.ToString(sqlDataReader["NomeCurso"])
            });
            if (num++ == 1)
              break;
          }
          return regimeList;
        }
      }
    }

    public List<alunoCursos> GetAlunoCursos(string numProcesso)
    {
      List<alunoCursos> alunoCursosList = new List<alunoCursos>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "GetAlunoCursoPlanoRamoIDMatricula";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            alunoCursosList.Add(new alunoCursos()
            {
              codCurso = Convert.ToString(sqlDataReader["codCurso"]),
              codPlano = Convert.ToString(sqlDataReader["codPlano"]),
              idRamo = Convert.ToString(sqlDataReader["IDRAMO"]),
              idMatricula = Convert.ToString(sqlDataReader["IDMATRICULA"]),
              nomeCurso = Convert.ToString(sqlDataReader["nome"]),
              siglaCurso = Convert.ToString(sqlDataReader["sigla"])
            });
        }
      }
      return alunoCursosList;
    }

    public List<alunoPlanoCurricular> GetAlunoNotas(string numProcesso)
    {
      List<alunoPlanoCurricular> alunoPlanoCurricularList = new List<alunoPlanoCurricular>();
      using (SqlConnection sqlConnection1 = new SqlConnection(this.myConnectionString))
      {
        using (SqlConnection sqlConnection2 = new SqlConnection(this.myConnectionString))
        {
          using (SqlConnection sqlConnection3 = new SqlConnection(this.myConnectionString))
          {
            using (SqlCommand sqlCommand = new SqlCommand())
            {
              sqlCommand.CommandText = "GetAlunoCursoPlanoRamoIDMatricula";
              sqlCommand.Connection = sqlConnection1;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
              sqlConnection1.Open();
              SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
              while (sqlDataReader1.Read())
              {
                int int32_1 = Convert.ToInt32(sqlDataReader1["IDMATRICULA"]);
                int int32_2 = Convert.ToInt32(sqlDataReader1["IDRAMO"]);
                Convert.ToInt32(sqlDataReader1["codCurso"]);
                sqlCommand.CommandText = "sp_planoNotas";
                sqlCommand.Connection = sqlConnection2;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@MATRID", SqlDbType.Int).Value = (object) int32_1;
                sqlConnection2.Open();
                SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
                while (sqlDataReader2.Read())
                {
                  alunoPlanoCurricular alunoPlanoCurricular = new alunoPlanoCurricular();
                  alunoPlanoCurricular.IDMatricula = Convert.ToInt32(sqlDataReader2["IDMatricula"]);
                  alunoPlanoCurricular.codCurso = Convert.ToInt32(sqlDataReader2["codCurso"]);
                  alunoPlanoCurricular.curso = Convert.ToString(sqlDataReader2["curso"]);
                  alunoPlanoCurricular.codDisciplina = Convert.ToInt32(sqlDataReader2["codDisciplina"]);
                  alunoPlanoCurricular.disciplina = Convert.ToString(sqlDataReader2["disciplina"]);
                  alunoPlanoCurricular.ano = Convert.ToInt32(sqlDataReader2["ano"]);
                  alunoPlanoCurricular.semestre = Convert.ToInt32(sqlDataReader2["semestre"]);
                  alunoPlanoCurricular.creditos = Convert.ToDouble(sqlDataReader2["creditos"]);
                  alunoPlanoCurricular.nota = Convert.ToString(sqlDataReader2["nota"]);
                  alunoPlanoCurricular.RefidOrdem = Convert.ToInt32(sqlDataReader2["idord"]);
                  alunoPlanoCurricular.ects = Convert.ToDouble(sqlDataReader2["ects"]);
                  alunoPlanoCurricular.IDRamo = Convert.ToInt32(sqlDataReader2["IDRamo"]);
                  alunoPlanoCurricular.TipoDiscip = Convert.ToInt32(sqlDataReader2["TipoDiscip"]);
                  if (alunoPlanoCurricular.TipoDiscip == 2)
                  {
                    sqlCommand.CommandText = "GetAlunoPlanoCurricularOpcionais";
                    sqlCommand.Connection = sqlConnection3;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Clear();
                    sqlCommand.Parameters.Add("@idMatricula", SqlDbType.Int).Value = (object) int32_1;
                    sqlCommand.Parameters.Add("@idRamo", SqlDbType.Int).Value = (object) int32_2;
                    sqlCommand.Parameters.Add("@idDisciplina", SqlDbType.Int).Value = (object) Convert.ToString(sqlDataReader2["codDisciplina"]);
                    sqlConnection3.Open();
                    SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
                    while (sqlDataReader3.Read())
                    {
                      grupoOpcional grupoOpcional = new grupoOpcional();
                      if (Convert.ToString(sqlDataReader3["nota"]) != "")
                      {
                        alunoPlanoCurricular.IP = Convert.ToString(sqlDataReader3["disciplina"]);
                        alunoPlanoCurricular.nota = Convert.ToString(sqlDataReader3["nota"]);
                      }
                    }
                    sqlConnection3.Close();
                  }
                  alunoPlanoCurricularList.Add(alunoPlanoCurricular);
                }
                sqlConnection2.Close();
              }
            }
          }
        }
      }
      return alunoPlanoCurricularList;
    }

    public List<alunoInscricoes> GetAlunoInscricoes(string numProcesso)
    {
      List<alunoInscricoes> alunoInscricoesList = new List<alunoInscricoes>();
      using (SqlConnection sqlConnection1 = new SqlConnection(this.myConnectionString))
      {
        using (SqlConnection sqlConnection2 = new SqlConnection(this.myConnectionString))
        {
          using (SqlCommand sqlCommand = new SqlCommand())
          {
            sqlCommand.CommandText = "GetAlunoCursoPlanoRamoIDMatricula";
            sqlCommand.Connection = sqlConnection1;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
            sqlConnection1.Open();
            SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
            while (sqlDataReader1.Read())
            {
              int int32 = Convert.ToInt32(sqlDataReader1["IDMATRICULA"]);
              Convert.ToInt32(sqlDataReader1["IDRAMO"]);
              Convert.ToInt32(sqlDataReader1["codCurso"]);
              sqlCommand.CommandText = "sp_CONS_ALUNOS_ExamesInscritos_AnoLectivo";
              sqlCommand.Connection = sqlConnection2;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.Parameters.Clear();
              sqlCommand.Parameters.Add("@MATRID", SqlDbType.Int).Value = (object) int32;
              sqlConnection2.Open();
              SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
              while (sqlDataReader2.Read())
                alunoInscricoesList.Add(new alunoInscricoes()
                {
                  TipoExame = Convert.ToString(sqlDataReader2["TipoExame"]),
                  IDMatricula = Convert.ToInt32(sqlDataReader2["IDMatricula"]),
                  nomedisciplina = Convert.ToString(sqlDataReader2["nomedisciplina"]),
                  creditos = Convert.ToDouble(sqlDataReader2["creditos"]),
                  ects = Convert.ToDouble(sqlDataReader2["ects"]),
                  anoLectivo = Convert.ToInt32(sqlDataReader2["anoLectivo"]),
                  anoCurricular = Convert.ToInt32(sqlDataReader2["anoCurricular"]),
                  semestre = Convert.ToInt32(sqlDataReader2["semestre"]),
                  codDisciplina = Convert.ToInt32(sqlDataReader2["codDisciplina"]),
                  dataInscricao = Convert.ToString(sqlDataReader2["dataInscricao"]),
                  primeira_vez = Convert.ToInt32(sqlDataReader2["primeira_vez"]),
                  regAluno = Convert.ToInt32(sqlDataReader2["regAluno"]),
                  validade = Convert.ToInt32(sqlDataReader2["validade"]),
                  epocaexame = Convert.ToInt32(sqlDataReader2["epocaexame"]),
                  departamento = Convert.ToString(sqlDataReader2["departamento"]),
                  codCurso = Convert.ToInt32(sqlDataReader2["codCurso"]),
                  curso = Convert.ToString(sqlDataReader2["curso"])
                });
              sqlConnection2.Close();
            }
          }
        }
      }
      return alunoInscricoesList;
    }

    public List<alunoPlanoCurricular> GetAlunoPlanoCurricular(string numProcesso)
    {
      List<alunoPlanoCurricular> alunoPlanoCurricularList = new List<alunoPlanoCurricular>();
      using (SqlConnection sqlConnection1 = new SqlConnection(this.myConnectionString))
      {
        using (SqlConnection sqlConnection2 = new SqlConnection(this.myConnectionString))
        {
          using (SqlConnection sqlConnection3 = new SqlConnection(this.myConnectionString))
          {
            using (SqlCommand sqlCommand = new SqlCommand())
            {
              sqlCommand.CommandText = "GetAlunoCursoPlanoRamoIDMatricula";
              sqlCommand.Connection = sqlConnection1;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
              sqlConnection1.Open();
              SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
              while (sqlDataReader1.Read())
              {
                int int32_1 = Convert.ToInt32(sqlDataReader1["IDMATRICULA"]);
                int int32_2 = Convert.ToInt32(sqlDataReader1["IDRAMO"]);
                Convert.ToInt32(sqlDataReader1["codCurso"]);
                sqlCommand.CommandText = "sp_planoNotas";
                sqlCommand.Connection = sqlConnection2;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@MATRID", SqlDbType.Int).Value = (object) int32_1;
                sqlConnection2.Open();
                SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
                while (sqlDataReader2.Read())
                {
                  alunoPlanoCurricular alunoPlanoCurricular = new alunoPlanoCurricular();
                  if (Convert.ToString(sqlDataReader2["tipodiscip"]) != "3")
                  {
                    alunoPlanoCurricular.IDMatricula = Convert.ToInt32(sqlDataReader2["IDMatricula"]);
                    alunoPlanoCurricular.codDisciplina = Convert.ToInt32(sqlDataReader2["codDisciplina"]);
                    alunoPlanoCurricular.disciplina = Convert.ToString(sqlDataReader2["disciplina"]);
                    alunoPlanoCurricular.ano = Convert.ToInt32(sqlDataReader2["ano"]);
                    alunoPlanoCurricular.semestre = Convert.ToInt32(sqlDataReader2["semestre"]);
                    alunoPlanoCurricular.creditos = Convert.ToDouble(sqlDataReader2["creditos"]);
                    alunoPlanoCurricular.RefidOrdem = Convert.ToInt32(sqlDataReader2["idord"]);
                    alunoPlanoCurricular.ects = Convert.ToDouble(sqlDataReader2["ects"]);
                    alunoPlanoCurricular.IDRamo = Convert.ToInt32(sqlDataReader2["IDRamo"]);
                    alunoPlanoCurricular.TipoDiscip = Convert.ToInt32(sqlDataReader2["TipoDiscip"]);
                    if (alunoPlanoCurricular.TipoDiscip == 2)
                    {
                      sqlCommand.CommandText = "GetAlunoPlanoCurricularOpcionais";
                      sqlCommand.Connection = sqlConnection3;
                      sqlCommand.CommandType = CommandType.StoredProcedure;
                      sqlCommand.Parameters.Clear();
                      sqlCommand.Parameters.Add("@idMatricula", SqlDbType.Int).Value = (object) int32_1;
                      sqlCommand.Parameters.Add("@idRamo", SqlDbType.Int).Value = (object) int32_2;
                      sqlCommand.Parameters.Add("@idDisciplina", SqlDbType.Int).Value = (object) Convert.ToString(sqlDataReader2["codDisciplina"]);
                      sqlConnection3.Open();
                      SqlDataReader sqlDataReader3 = sqlCommand.ExecuteReader();
                      while (sqlDataReader3.Read())
                        alunoPlanoCurricular.grupoOpcional.Add(new grupoOpcional()
                        {
                          codDisciplinaOp = Convert.ToString(sqlDataReader3["codDisciplina"]),
                          disciplinaOp = Convert.ToString(sqlDataReader3["disciplina"])
                        });
                      sqlConnection3.Close();
                    }
                    alunoPlanoCurricularList.Add(alunoPlanoCurricular);
                  }
                }
              }
            }
          }
        }
      }
      return alunoPlanoCurricularList;
    }

    public List<AlunoSIGACAD> GetAlunoDadosPessoais(string numProcesso)
    {
      List<AlunoSIGACAD> alunoSigacadList = new List<AlunoSIGACAD>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "GetAlunoDadosPessoais";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          int num = 0;
          while (sqlDataReader.Read())
          {
            alunoSigacadList.Add(new AlunoSIGACAD()
            {
              NumProcesso = Convert.ToString(sqlDataReader["Numero"]),
              Nome = Convert.ToString(sqlDataReader["Nome"]),
              Sexo = Convert.ToString(sqlDataReader["Sexo"]),
              DataNasc = Convert.ToDateTime(sqlDataReader["DataNasc"]),
              biDataEmissao = Convert.ToDateTime(sqlDataReader["biDataEmissao"]),
              NomePai = Convert.ToString(sqlDataReader["Pai"]),
              NomeMae = Convert.ToString(sqlDataReader["Mae"]),
              Nacionalidade = Convert.ToString(sqlDataReader["Nacionalidade"]),
              NumID = Convert.ToString(sqlDataReader["BI"]),
              Telefone = Convert.ToString(sqlDataReader["Telefone"]),
              Telemovel = Convert.ToString(sqlDataReader["Telemovel"]),
              Endereco = Convert.ToString(sqlDataReader["Endereco"]),
              EMail = Convert.ToString(sqlDataReader["EMail"]),
              Localidade = Convert.ToString(sqlDataReader["Localidade"]),
              CodPostal = Convert.ToString(sqlDataReader["CodPostal"]),
              codResideDistrito = Convert.ToString(sqlDataReader["CodDistrito"]),
              codResideConcelho = Convert.ToString(sqlDataReader["CodConcelho"]),
              Observ = Convert.ToString(sqlDataReader["Oberv"]),
              nib = Convert.ToString(sqlDataReader["NIB"]),
              nif = Convert.ToString(sqlDataReader["ncontribuinte"]),
              codRegime = Convert.ToString(sqlDataReader["codRegime"]),
              nomeRegime = Convert.ToString(sqlDataReader["nomeRegime"])
            });
            if (num++ == 1)
              break;
          }
        }
      }
      return alunoSigacadList;
    }

    public List<propinasAluno> GetAlunoPropinas(string numProcesso)
    {
      List<propinasAluno> propinasAlunoList = new List<propinasAluno>();
      using (new SqlConnection(this.myConnectionString))
      {
        using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
        {
          using (SqlCommand sqlCommand = new SqlCommand())
          {
            sqlCommand.CommandText = "sp_CONS_ALUNOS_Propinas2";
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Parameters.Clear();
            sqlCommand.Parameters.Add("@num", SqlDbType.Int).Value = (object) numProcesso;
            sqlConnection.Open();
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
              propinasAlunoList.Add(new propinasAluno()
              {
                anoLectivo = Convert.ToString(sqlDataReader["anoLectivo"]),
                codCurso = Convert.ToString(sqlDataReader["codCurso"]),
                curso = Convert.ToString(sqlDataReader["curso"]),
                descr = Convert.ToString(sqlDataReader["descr"]),
                referencia = Convert.ToString(sqlDataReader["referencia"]),
                valor = string.Format("{0:0.00}", (object) Convert.ToDouble(sqlDataReader["valor"])),
                estado = Convert.ToString(sqlDataReader["designacao"]),
                datalimitepag = Convert.ToString(sqlDataReader["datalimitepag"]),
                entidade = Convert.ToString(sqlDataReader["entidade"])
              });
            sqlConnection.Close();
          }
        }
      }
      return propinasAlunoList;
    }

    public List<propinasAluno> GetAlunoPropinasPagas(string numProcesso)
    {
      List<propinasAluno> propinasAlunoList = new List<propinasAluno>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "stp_Propinas_Pagas_S";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Clear();
          sqlCommand.Parameters.Add("@numero", SqlDbType.Int).Value = (object) numProcesso;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            propinasAlunoList.Add(new propinasAluno()
            {
              numero = Convert.ToString(sqlDataReader["numero"]),
              anoLectivo = Convert.ToString(sqlDataReader["anoLectivo"]),
              codCurso = Convert.ToString(sqlDataReader["codCurso"]),
              curso = Convert.ToString(sqlDataReader["curso"]),
              prestacao = Convert.ToString(sqlDataReader["prest"]),
              valor = string.Format("{0:0.00}", (object) Convert.ToDouble(sqlDataReader["valor"])),
              moeda = Convert.ToString(sqlDataReader["moeda"]),
              datalimitepag = Convert.ToString(sqlDataReader["datalimite"]),
              datapagamento = Convert.ToString(sqlDataReader["datapagamento"]),
              pendente = Convert.ToString(sqlDataReader["pendente"]),
              datadevolucao = Convert.ToString(sqlDataReader["datadevolucao"]),
              dataanulacao = Convert.ToString(sqlDataReader["dataanulacao"])
            });
          sqlConnection.Close();
        }
      }
      return propinasAlunoList;
    }

    public List<alunoPlanoCurricular> GetAlunoUCEmFalta(string numProcesso)
    {
      List<alunoPlanoCurricular> alunoPlanoCurricularList = new List<alunoPlanoCurricular>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (new SqlConnection(this.myConnectionString))
        {
          using (new SqlConnection(this.myConnectionString))
          {
            using (SqlCommand sqlCommand = new SqlCommand())
            {
              sqlCommand.CommandText = "GetAlunoCursoPlanoRamoIDMatricula";
              sqlCommand.Connection = sqlConnection;
              sqlCommand.CommandType = CommandType.StoredProcedure;
              sqlCommand.Parameters.Add("@Numero", SqlDbType.Int).Value = (object) numProcesso;
              sqlConnection.Open();
              SqlDataReader sqlDataReader1 = sqlCommand.ExecuteReader();
              while (sqlDataReader1.Read())
              {
                int int32_1 = Convert.ToInt32(sqlDataReader1["IDMATRICULA"]);
                int int32_2 = Convert.ToInt32(sqlDataReader1["IDRAMO"]);
                int int32_3 = Convert.ToInt32(sqlDataReader1["codCurso"]);
                sqlCommand.CommandText = "GetAlunoUCEmFalta";
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@idMatricula", SqlDbType.Int).Value = (object) int32_1;
                sqlCommand.Parameters.Add("@idRamo", SqlDbType.Int).Value = (object) int32_2;
                sqlConnection.Open();
                SqlDataReader sqlDataReader2 = sqlCommand.ExecuteReader();
                while (sqlDataReader2.Read())
                  alunoPlanoCurricularList.Add(new alunoPlanoCurricular()
                  {
                    IDMatricula = Convert.ToInt32(sqlDataReader2["IDMatricula"]),
                    codDisciplina = Convert.ToInt32(sqlDataReader2["codDisciplina"]),
                    disciplina = Convert.ToString(sqlDataReader2["disciplina"]),
                    ano = Convert.ToInt32(sqlDataReader2["ano"]),
                    semestre = Convert.ToInt32(sqlDataReader2["semestre"]),
                    creditos = Convert.ToDouble(sqlDataReader2["creditos"]),
                    primeira_vez = Convert.ToInt32(sqlDataReader2["frequentou"]),
                    IP = "",
                    codCurso = int32_3,
                    RefidOrdem = Convert.ToInt32(sqlDataReader2["idord"]),
                    ects = Convert.ToDouble(sqlDataReader2["ects"]),
                    IDRamo = Convert.ToInt32(sqlDataReader2["IDRamo"]),
                    TipoDiscip = Convert.ToInt32(sqlDataReader2["TipoDiscip"]),
                    inscrito = Convert.ToInt32(sqlDataReader2["inscrito"]),
                    transferido = Convert.ToInt32(sqlDataReader2["transferido"]),
                    grupoOpcional = !(Convert.ToString(sqlDataReader2["tipodiscip"]) == "2") ? (List<grupoOpcional>) null : (List<grupoOpcional>) null
                  });
              }
            }
          }
        }
      }
      return alunoPlanoCurricularList;
    }

    public List<NomeAlunoCursoAluno> GetNomeAlunoCursoAluno(string numMec)
    {
      List<NomeAlunoCursoAluno> nomeAlunoCursoAlunoList = new List<NomeAlunoCursoAluno>();
      int num = int.Parse(numMec);
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "GetNomeAlunoCursoAluno";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@numero", SqlDbType.Int).Value = (object) num;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            nomeAlunoCursoAlunoList.Add(new NomeAlunoCursoAluno()
            {
              nome = Convert.ToString(sqlDataReader["nome"]),
              codCurso = Convert.ToString(sqlDataReader["codCurso"]),
              siglaCurso = Convert.ToString(sqlDataReader["siglaCurso"]),
              curso = Convert.ToString(sqlDataReader["curso"]),
              numero = Convert.ToInt32(sqlDataReader["numero"]),
              datainscricao = Convert.ToString(sqlDataReader["data"]),
              anoCurricular = Convert.ToString(sqlDataReader["anoCurricular"]),
              anoInscricao = Convert.ToString(sqlDataReader["anoInscricao"])
            });
        }
        sqlConnection.Close();
      }
      return nomeAlunoCursoAlunoList;
    }

    public List<dividasAluno> GetAlunoDividas(string numProcesso)
    {
      List<dividasAluno> dividasAlunoList = new List<dividasAluno>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "stp_DividasAluno_S";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@numero", SqlDbType.Int).Value = (object) numProcesso;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            dividasAlunoList.Add(new dividasAluno()
            {
              anoLectivo = Convert.ToString(sqlDataReader["anoLectivo"]),
              datadivida = Convert.ToString(sqlDataReader["datadivida"]),
              valordivida = Convert.ToString(sqlDataReader["valordivida"]),
              descritivo = Convert.ToString(sqlDataReader["descritivo"]),
              nometipodivida = Convert.ToString(sqlDataReader["nometipodivida"]),
              datalimite = Convert.ToString(sqlDataReader["datalimite"])
            });
          sqlConnection.Close();
        }
      }
      return dividasAlunoList;
    }

    public List<inscricoesAlunosUcCurso> GetInscricaoAlunosUCCurso(string numero, string codCurso)
    {
      List<inscricoesAlunosUcCurso> inscricoesAlunosUcCursoList = new List<inscricoesAlunosUcCurso>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "stp_S_InscricaoAlunosUCCurso";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Clear();
          sqlCommand.Parameters.Add("@codUC", SqlDbType.Int).Value = (object) numero;
          sqlCommand.Parameters.Add("@codCurso", SqlDbType.Int).Value = (object) codCurso;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            inscricoesAlunosUcCursoList.Add(new inscricoesAlunosUcCurso()
            {
              numero = Convert.ToString(sqlDataReader["numero"]),
              nome = Convert.ToString(sqlDataReader["nome"]),
              ano = Convert.ToString(sqlDataReader["ano"]),
              semestre = Convert.ToString(sqlDataReader["semestreinscricao"]),
              anoLectivo = Convert.ToString(sqlDataReader["anoLectivo"]),
              dataInscricao = Convert.ToString(sqlDataReader["dataInscricao"]),
              tipoExame = Convert.ToString(sqlDataReader["tipoExame"]),
              epocaExame = Convert.ToString(sqlDataReader["epocaExame"]),
              primeiraVez = Convert.ToString(sqlDataReader["primeira_vez"])
            });
          sqlConnection.Close();
        }
      }
      return inscricoesAlunosUcCursoList;
    }

    public List<alunoFromSIGACAD> GetAlunoFromSIGACAD(string numMec)
    {
      List<alunoFromSIGACAD> alunoFromSigacadList = new List<alunoFromSIGACAD>();
      using (SqlConnection sqlConnection = new SqlConnection(this.myConnectionString))
      {
        using (SqlCommand sqlCommand = new SqlCommand())
        {
          sqlCommand.CommandText = "stp_Aluno_FROM_SIGACAD_S";
          sqlCommand.Connection = sqlConnection;
          sqlCommand.CommandType = CommandType.StoredProcedure;
          sqlCommand.Parameters.Add("@NumeroIn", SqlDbType.Int).Value = (object) numMec;
          sqlConnection.Open();
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
          while (sqlDataReader.Read())
            alunoFromSigacadList.Add(new alunoFromSIGACAD()
            {
              numeroAluno = Convert.ToString(sqlDataReader["numero"]),
              nomeAluno = Convert.ToString(sqlDataReader["nome"]),
              telefone = Convert.ToString(sqlDataReader["telefone"]),
              email = Convert.ToString(sqlDataReader["e_mail"])
            });
          return alunoFromSigacadList;
        }
      }
    }
  }
}
