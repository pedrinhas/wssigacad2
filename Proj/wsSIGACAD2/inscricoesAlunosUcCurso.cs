﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.inscricoesAlunosUcCurso
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class inscricoesAlunosUcCurso
  {
    [DataMember]
    public string numero;
    [DataMember]
    public string nome;
    [DataMember]
    public string ano;
    [DataMember]
    public string semestre;
    [DataMember]
    public string anoLectivo;
    [DataMember]
    public string dataInscricao;
    [DataMember]
    public string tipoExame;
    [DataMember]
    public string epocaExame;
    [DataMember]
    public string primeiraVez;
  }
}
