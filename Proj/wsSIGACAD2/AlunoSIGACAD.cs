﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.AlunoSIGACAD
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System;
using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class AlunoSIGACAD
  {
    [DataMember]
    public string NumProcesso;
    [DataMember]
    public string IDEstab;
    [DataMember]
    public string Nome;
    [DataMember]
    public string NumID;
    [DataMember]
    public string biArquivo;
    [DataMember]
    public DateTime biDataEmissao;
    [DataMember]
    public string TipoID;
    [DataMember]
    public string NomePai;
    [DataMember]
    public string NomeMae;
    [DataMember]
    public DateTime DataNasc;
    [DataMember]
    public string Sexo;
    [DataMember]
    public string codResideDistrito;
    [DataMember]
    public string codResideConcelho;
    [DataMember]
    public string codNaturalDistrito;
    [DataMember]
    public string codNaturalConcelho;
    [DataMember]
    public string codNaturalFreguesia;
    [DataMember]
    public string Telefone;
    [DataMember]
    public string Telemovel;
    [DataMember]
    public string EMail;
    [DataMember]
    public string Endereco;
    [DataMember]
    public string Localidade;
    [DataMember]
    public string CodPostal;
    [DataMember]
    public string repnif;
    [DataMember]
    public string Observ;
    [DataMember]
    public string nif;
    [DataMember]
    public string nib;
    [DataMember]
    public string codRegime;
    [DataMember]
    public string nomeRegime;
    [DataMember]
    public string Naturalidade;
    [DataMember]
    public string Nacionalidade;
    [DataMember]
    public string Nacionalidade2;
    [DataMember]
    public string AltData;
    [DataMember]
    public string AltUser;
    [DataMember]
    public string Ano;
    [DataMember]
    public string Estab;
    [DataMember]
    public string Curso;
    [DataMember]
    public string Ciclo;
    [DataMember]
    public string Ramo;
    [DataMember]
    public string AnoCurricular;
    [DataMember]
    public string PrimeiraVez;
    [DataMember]
    public string NumInsc;
    [DataMember]
    public string RegimeFreq;
    [DataMember]
    public string TempoParcial;
    [DataMember]
    public string FreqOutros;
    [DataMember]
    public string ProgComunitario;
    [DataMember]
    public string FormaIngresso;
    [DataMember]
    public string InscAnt_estab;
    [DataMember]
    public string InscAnt_estnome;
    [DataMember]
    public string NumInscAnt;
    [DataMember]
    public string Ingresso_nota;
    [DataMember]
    public string Ingresso_opcao;
    [DataMember]
    public string NumCandAnt;
    [DataMember]
    public string ResidePais;
    [DataMember]
    public string AlunoDeslocado;
    [DataMember]
    public string EstadoCivil;
    [DataMember]
    public string NivelEscolar_Pai;
    [DataMember]
    public string NivelEscolar_Mae;
    [DataMember]
    public string SitProf_Pai;
    [DataMember]
    public string SitProf_Mae;
    [DataMember]
    public string SitProf_Aluno;
    [DataMember]
    public string Profissao_Pai;
    [DataMember]
    public string Profissao_Mae;
    [DataMember]
    public string Profissao_Aluno;
    [DataMember]
    public string Bolseiro;
    [DataMember]
    public string Bolseiro_Inst;
    [DataMember]
    public string EstudanteTrabalhador;
    [DataMember]
    public string TipoEstabSec;
    [DataMember]
    public string Retencao_PreUniv;
    [DataMember]
    public string HabilAnt_grau;
    [DataMember]
    public string HabilAnt_graunome;
    [DataMember]
    public string HabilAnt_estab;
    [DataMember]
    public string HabilAnt_estnome;
    [DataMember]
    public string HabilAnt_curso;
    [DataMember]
    public string HabilAnt_curnome;
    [DataMember]
    public string HabilAnt_AnoConc;
    [DataMember]
    public string HabilAnt_Pais;
  }
}
