﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.alunoPlanoCurricular
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class alunoPlanoCurricular
  {
    [DataMember]
    public List<wsSIGACAD2.grupoOpcional> grupoOpcional = new List<wsSIGACAD2.grupoOpcional>();
    [DataMember]
    public int IDMatricula;
    [DataMember]
    public int codDisciplina;
    [DataMember]
    public string disciplina;
    [DataMember]
    public int ano;
    [DataMember]
    public int semestre;
    [DataMember]
    public double creditos;
    [DataMember]
    public int epocaexame;
    [DataMember]
    public int primeira_vez;
    [DataMember]
    public string IP;
    [DataMember]
    public int codCurso;
    [DataMember]
    public string curso;
    [DataMember]
    public int RefidOrdem;
    [DataMember]
    public double ects;
    [DataMember]
    public int IDRamo;
    [DataMember]
    public int TipoDiscip;
    [DataMember]
    public int inscrito;
    [DataMember]
    public int transferido;
    [DataMember]
    public string nota;
  }
}
