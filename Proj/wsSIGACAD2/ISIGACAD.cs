﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.ISIGACAD
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Collections.Generic;
using System.Data;
using System.ServiceModel;

namespace wsSIGACAD2
{
  [ServiceContract]
  public interface ISIGACAD
  {
    [OperationContract]
    void DoWork();

    [OperationContract]
    DataTable lerRegime(string numMec);

    [OperationContract]
    List<Regime> GetAlunoRegime(string numMec);

    [OperationContract]
    List<alunoPlanoCurricular> GetAlunoNotas(string numMec);

    [OperationContract]
    List<alunoInscricoes> GetAlunoInscricoes(string numMec);

    [OperationContract]
    List<alunoPlanoCurricular> GetAlunoPlanoCurricular(string numMec);

    [OperationContract]
    List<AlunoSIGACAD> GetAlunoDadosPessoais(string numMec);

    [OperationContract]
    List<propinasAluno> GetAlunoPropinas(string numMec);

    [OperationContract]
    List<dividasAluno> GetAlunoDividas(string numMec);

    [OperationContract]
    List<propinasAluno> GetAlunoPropinasPagas(string numMec);

    [OperationContract]
    List<alunoPlanoCurricular> GetAlunoUCEmFalta(string numMec);

    [OperationContract]
    List<NomeAlunoCursoAluno> GetNomeAlunoCursoAluno(string numMec);

    [OperationContract]
    List<alunoCursos> GetAlunoCursos(string numMec);

    [OperationContract]
    List<inscricoesAlunosUcCurso> GetInscricaoAlunosUCCurso(string codUC, string codCurso);

    [OperationContract]
    List<alunoFromSIGACAD> GetAlunoFromSIGACAD(string numMec);
  }
}
