﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.NomeAlunoCursoAluno
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class NomeAlunoCursoAluno
  {
    [DataMember]
    public string nome;
    [DataMember]
    public string curso;
    [DataMember]
    public string codCurso;
    [DataMember]
    public string siglaCurso;
    [DataMember]
    public int numero;
    [DataMember]
    public string datainscricao;
    [DataMember]
    public string anoCurricular;
    [DataMember]
    public string anoInscricao;
  }
}
