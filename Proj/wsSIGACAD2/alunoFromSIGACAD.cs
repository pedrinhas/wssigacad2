﻿// Decompiled with JetBrains decompiler
// Type: wsSIGACAD2.alunoFromSIGACAD
// Assembly: wsSIGACAD2, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: DFC127FA-3E60-41DE-ADC8-51C01A07E17D
// Assembly location: A:\Git repositories\wsSIGACAD2\wsSIGACAD2\bin\wsSIGACAD2.dll

using System.Runtime.Serialization;

namespace wsSIGACAD2
{
  [DataContract]
  public class alunoFromSIGACAD
  {
    [DataMember]
    public string numeroAluno;
    [DataMember]
    public string nomeAluno;
    [DataMember]
    public string telefone;
    [DataMember]
    public string email;
  }
}
