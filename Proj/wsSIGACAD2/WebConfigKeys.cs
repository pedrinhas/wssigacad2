﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace wsSIGACAD2
{
    public static class WebConfigKeys
    {
        public static class ConnectionStrings
        {
            public static string Apps2
            {
                get
                {
                    return ConfigurationManager.ConnectionStrings["conn_apps2"].ToString();
                }
            }
        }

        public static class Keys
        {
        }
    }
}
